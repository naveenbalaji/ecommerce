import React, { useState } from 'react'
// import Fab from '@material-ui/core/Fab';
// import HomeIcon from '@material-ui/icons/Home';
// import SearchIcon from '@material-ui/icons/Search';
// import LocalMallIcon from '@material-ui/icons/LocalMall';
// import PersonIcon from '@material-ui/icons/Person';
import { createMuiTheme, ThemeProvider } from '@material-ui/core/styles';
import { Link } from 'react-router-dom';
import './main.css';

const theme = createMuiTheme({
    palette: {
        primary: {
            main: "#ffffff",
        },
        secondary: {
            main: '#ffffff',
        },
    },
});
const Bottombar = () => {
    // const [Homeiconset, Sethomeiconset] = useState('');
    // const [Searchiconset, Setsearchiconset] = useState('');
    // const [Carticonset, Setcarticonset] = useState('');
    // const [Profileiconset, Setprofileiconset] = useState('');

    // const HomehandleEvent = (e) => {
    //     Sethomeiconset("Home");
    // }
    // const SearchhandleEvent = (e) => {
    //     Setsearchiconset("Search");
    // }
    // const CarthandleEvent = (e) => {
    //     Setcarticonset("Cart");
    // }
    // const ProfilehandleEvent = (e) => {
    //     Setprofileiconset("Profile");
    // }
    return (
        <div className="mycontainer">
            <div className="container">
                <div className="row ">
                   <nav className="tab-content navbar">
        
                        <div className="tab">
                            <Link to='/' style={{ textDecoration: 'none'}}>
                               <i className="fas fa-home" ></i>
                               <p>Home</p>
                            </Link>
                        </div>
                        <div className="tab">
                            <Link to='/search'>
                               <i className="fas fa-search" style={{ textDecoration: 'none'}}></i>
                               <p>Search</p>
                            </Link>
                        </div>
                        <div className="tab">
                            <Link to='/cart'>
                            <i className="fas fa-shopping-bag" style={{ textDecoration: 'none'}}></i>
                            <p>Cart</p>
                            </Link>
                        </div>
                        <div className="tab">
                            <Link to='/profile'>
                            <i className="fas fa-user" style={{ textDecoration: 'none'}}></i>
                            <p>Profile</p>
                            </Link>
                        </div>
                      
                    </nav>
                </div>
            </div>
        </div>
    )
}

export default Bottombar
