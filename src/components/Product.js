import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import Bottombar from './Bottombar';
import './main.css';
import { useHistory } from 'react-router-dom';


const Product = (props) => {
    const [productData, SetproductData] = useState([]);
    const history = useHistory();
    useEffect(() => {
        getProducts();
    }, [])
    console.log(props.location.productData);
    const getProducts = async () => {
        Axios.get(`http://localhost:8000/product/${props.location.productData}`).then(res => {
            console.log(res.data.data);
            SetproductData(res.data.data);
        })
    }

    const handleBack = () => {
        history.goBack();
    }

    const handleCart = () => {
        history.push('/cart');
    }
    return (
        <div>
            <div className="container p-2">
                <div className="row">
                    <div className="col">
                        <div className="navitem">
                            <p onClick={handleBack}><i className="fas fa-arrow-left fa-lg" ></i></p>
                            <p onClick={handleCart}><i className="fas fa-shopping-bag fa-lg"></i></p>
                        </div>
                    </div>
                </div>
            </div>

            <div className="carddecks">
                {productData.map(item => (
                    <div className="container" key={item.imageId}>
                        <div className="row">
                            <div className="col">
                                <div className="card productcard">
                                    <div className="mycolum">
                                        <div className="rowcol">
                                            <h4>Chair</h4><br />
                                            <p>FROM</p><br />
                                            <p>$289.99</p>
                                            <br />
                                            <p>Available colors</p>
                                        </div>
                                        <img src={item.productImagePath} className="img-fluid myimage" alt="Responsive image" />
                                    </div>
                                </div>
                            </div>
                            <div className="card-body">
                                <p className="card-text">{item.categoryName}</p>
                            </div>
                        </div>
                    </div>
                ))}
            </div>
            <Bottombar />
        </div>
    );
}

export default Product;
