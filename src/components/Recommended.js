import React,{useState,useEffect} from 'react';
import Axios from 'axios';
import './main.css';

const Recommended = () => {
    const [recommendedItems,setrecommendedItems]=useState([])

    useEffect( ()=>{
        Axios.get('http://localhost:8000/recommended').then(res=>{
            setrecommendedItems(res.data.data)
    })   
    },[])

    return (
        <div>
            <div className="carddecks">
                {recommendedItems.map(item => (
                    <div key={item.imageId}>
                        <div className="card cardItem cardSpace" style={{ width: "10rem" }}>
                            <img className="card-img-top img-fluid rounded carddecks" src={item.productImagePath} alt={item.categoryName} />
                            <div className="card-body">
                                <p className="card-text">{item.categoryName}</p>
                            </div>
                        </div>
                    </div>
                ))}
            </div>
        </div>
    );
}

export default Recommended
