import React, { useState, useEffect } from 'react';
import Axios from 'axios';
import "./main.css";
import { useHistory } from 'react-router-dom';

const Items = (props) => {
    const [productData, SetproductData] = useState([]);
    const history = useHistory();
    useEffect(() => {
        getProducts();
    }, [])
    console.log(props.location.productData);
    const getProducts = async () => {
        Axios.get(`http://localhost:8000/category/get/${props.location.productData}`).then(res => {
            console.log(res.data.data);
            SetproductData(res.data.data);
        })
    }

    const getSpecificProduct = (product) => {
        history.push({
            pathname: '/product',
            productData: product,
        });
    }

    return (
        <div>
            <div className="container">
                <div className="row">
                    <div className="col">
                        <h1> Listing {props.location.productData} Products...</h1>
                        <div className="carddecks">
                            {productData.map(item => (
                                <div key={item.imageId} onClick={() => getSpecificProduct(item.groupId)}>
                                    <div className="card cardItem" style={{ width: "10rem" }}>
                                        <img className="card-img-top img-fluid rounded myproductimage" src={item.productImagePath} alt={item.categoryName} />
                                        <div className="card-body">
                                            <p className="card-text">{item.productName}</p>
                                        </div>
                                    </div>
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Items;
